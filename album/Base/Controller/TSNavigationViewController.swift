//
//  WSJNavigationViewController.swift
//  favoritealbum
//
//  Created by tonysing on 29/10/2021.
//

import UIKit

class TSNavigationViewController: UINavigationController {

    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        super.pushViewController(viewController, animated: animated)
    }
    
    @objc func back(){
        self.popViewController(animated: true)
    }
    
}
