//
//  TSBaseViewController.swift
//  favoritealbum
//
//  Created by tonysing on 29/10/2021.
//

import UIKit
import SnapKit
import DZNEmptyDataSet

class TSBaseViewController: UIViewController, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    //    MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.black]
        self.navigationController?.navigationBar.barTintColor = .white
    }
    
    
    // Cancel Editing when view disappear
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
        super.viewWillDisappear(animated)
    }
    
    //Default: back to previous, pls overrite if needed
    @objc func backTo() {
        self.navigationController?.popViewController(animated: true)
    }
    
    // Display empty image if empty list, pls child override me if need to change the empty image on it's screen
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return UIImage(named: "empty")
    }
    
}
