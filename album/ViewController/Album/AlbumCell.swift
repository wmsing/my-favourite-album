//
//  AlbumCell.swift
//  album
//
//  Created by tonysing on 31/10/2021.
//
import UIKit
import Kingfisher

protocol AlbumCellDelegate: AnyObject {
    func onTapBookmark(rowOfAlbum: Int)
}

class AlbumCell: UITableViewCell {
    
    private var titleLabel : UILabel!
    private var centerContainer:UIView!
    private var descLabel :UILabel!
    private var dateLabel :UILabel!
    private var albumImage :UIImageView!
    
    var bookmarkBtn :UIButton!
    weak var delegate: AlbumCellDelegate?
    var model = AlbumResultModel(){
        didSet{
            titleLabel.text = model.collectionName
            descLabel.text = model.artistName
            dateLabel.text = TSDateTime.isoDateToYyMmDd( from: model.releaseDate)
            albumImage.kf.setImage(with: URL(string: model.artworkUrl100), placeholder: UIImage(named: "default"))
            bookmarkBtn.setImage(UIImage(named: model.isBookmarked ? "bookmark" : "bookmark_cancel"), for: .normal)
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configUI () {
        contentView.backgroundColor = .white
        albumImage = TSView.imageView(frame: .zero, named: "default")
        centerContainer = TSView.view(frame: .zero, bgColor: .white)
        titleLabel = TSView.label(text: "", frame: .zero, textColor: .themeFontBlack, fontSize: parsePx(34), align: .left)
        descLabel = TSView.label(text: "", frame: .zero, textColor: .themeLightGray, fontSize: parsePx(20), align: .left)
        dateLabel = TSView.label(text: "", frame: .zero, textColor: .themeLightGray, fontSize: parsePx(20), align: .left)
        bookmarkBtn = UIButton(type: .system)
        bookmarkBtn.setImage(UIImage(named: "bookmark_cancel"), for: .normal)
        bookmarkBtn.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        bookmarkBtn.tintColor = .red
        bookmarkBtn.addTarget(self, action: #selector(bookMark), for: .touchUpInside)
        
        contentView.addSubview(albumImage)
        centerContainer.addSubview(titleLabel)
        centerContainer.addSubview(descLabel)
        centerContainer.addSubview(dateLabel)
        contentView.addSubview(centerContainer)
        contentView.addSubview(bookmarkBtn)
        
        // Constrain
        albumImage.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.height.equalTo(parsePx(200))
            make.width.equalTo(parsePx(200))
        }
        
        centerContainer.snp.makeConstraints { (make) in
            make.left.equalTo(albumImage.snp.right).offset(parsePx(10))
            make.top.equalToSuperview().offset(parsePx(10))
            make.height.equalTo(parsePx(200))
            make.width.equalTo(parsePx(400))
        }
        titleLabel.snp.makeConstraints { (make) in
            make.left.top.width.equalToSuperview()
            make.height.equalTo(parsePx(50))
        }
        descLabel.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom)
            make.left.width.equalToSuperview()
            make.height.equalTo(parsePx(50))
        }
        
        dateLabel.snp.makeConstraints { (make) in
            make.top.equalTo(descLabel.snp.bottom).offset(parsePx(20))
            make.left.width.equalToSuperview()
            make.height.equalTo(parsePx(50))
        }

        bookmarkBtn.snp.makeConstraints { (make) in
            make.centerY.equalTo(contentView)
            make.width.height.equalTo(parsePx(100))
            make.right.equalTo(contentView).offset(-parsePx(20))
        }
    }
    
    @objc private func bookMark(sender: UIButton){
//        print(sender.tag)
        delegate?.onTapBookmark(rowOfAlbum: sender.tag);
    }
    
}
