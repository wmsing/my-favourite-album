//
//  AlbumVC.swift
//  album
//
//  Created by tonysing on 31/10/2021.
//

import UIKit
import Alamofire

class AlbumVC: TSBaseViewController{
    private var albumTableView = UITableView(frame:.zero, style: .plain)
    private let cell = "AlbumCell"
    private var albumModels = [AlbumResultModel](){
        didSet{
            albumTableView.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configUI()
        loadData()
        addObserver()
       
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    private func configUI() {
        setNavBar()
        configAlbumTableView()
    }
    
    private func setNavBar() {
        
        navigationItem.title = "Album"
        let btn = createNavigationItemWithTitle(str: "", imageName: "bookmarks", selector: #selector(redirectToBookmarkVC(sender:)),isLeft: false)
        btn.tintColor = .red
    }
    
    private func loadData(){
        TSHud.loading()
        
        TSNetwork.get("search?term=jack+johnson&entity=album") { result in
            guard let model = AlbumModel.deserialize(from: result as? [String : Any]) else { return }
            print(model.results)
            self.albumModels = model.results;
        } fail: { error in
            print(error)
        } complete: {
            TSHud.disMiss()
        }
    }
    

}

// MARK: - User Action
extension AlbumVC{
    @objc func redirectToBookmarkVC(sender: UIButton) {
        let pushVC = BookMarkVC()
        pushVC.albumModels = albumModels.filter({ albumResultModel in
            return albumResultModel.isBookmarked
        })
        pushVC.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(pushVC, animated: true)
    }
    
    private func addObserver(){
        NotificationCenter.addObserver(observer: self, selector: #selector(removeBookmarkFromBookmarkList), name: .removeBookmark)
    }
    
    // Listening notifiction from bookmark page...
    @objc func removeBookmarkFromBookmarkList(notification: Notification){
        print("AlbumVC removeBookmark..")
        guard let model = notification.object as? AlbumResultModel else{return}
        if let modelRemovedBookmark = albumModels.first(where: {$0.collectionId == model.collectionId}) {
            modelRemovedBookmark.isBookmarked = model.isBookmarked;
            albumTableView.reloadData()
        }
    }
    
}

// MARK: - TableView
extension AlbumVC: UITableViewDelegate,UITableViewDataSource{
    private func configAlbumTableView(){
        // Register
        albumTableView.delegate = self
        albumTableView.dataSource = self
        albumTableView.emptyDataSetSource = self
        albumTableView.emptyDataSetDelegate = self
        albumTableView.register(AlbumCell.self, forCellReuseIdentifier: cell)

        view.addSubview(albumTableView)
        
        // Constrain
        albumTableView.snp.makeConstraints { (make) in
            make.top.equalTo(self.view).offset(navBarHeight())
            make.left.right.bottom.equalTo(self.view)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return albumModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cell, for: indexPath) as! AlbumCell
        cell.selectionStyle = .none
        cell.delegate=self
        cell.bookmarkBtn.tag = indexPath.row
        cell.model = albumModels[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return parsePx(200)
    }
}

// MARK: - AlbumCell Delegate
extension AlbumVC: AlbumCellDelegate{
    func onTapBookmark(rowOfAlbum: Int) {
        albumModels[rowOfAlbum].isBookmarked = !albumModels[rowOfAlbum].isBookmarked;
        albumTableView.reloadData()
    }
}
