//
//  BookMarkVC.swift
//  album
//
//  Created by tonysing on 31/10/2021.
//

import Foundation
import UIKit

class BookMarkVC: TSBaseViewController{
    private let cell = "AlbumCell"
    private var albumTableView = UITableView(frame:.zero, style: .plain)
    
    var albumModels = [AlbumResultModel](){
        didSet{
            albumTableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Bookmark"
        configUI()
    }

    private func configUI(){
        configTableView()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

// MARK: - TableView
extension BookMarkVC: UITableViewDelegate, UITableViewDataSource{
    private func configTableView(){
        // Register
        albumTableView.delegate = self
        albumTableView.dataSource = self
        albumTableView.emptyDataSetSource = self
        albumTableView.emptyDataSetDelegate = self
        albumTableView.register(AlbumCell.self, forCellReuseIdentifier: cell)
        view.addSubview(albumTableView)
        
        // Constrain
        albumTableView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(60)
            make.left.right.bottom.equalToSuperview()
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        albumModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cell, for: indexPath) as! AlbumCell
        cell.delegate=self
        cell.bookmarkBtn.tag = indexPath.row
        cell.model = albumModels[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return parsePx(200)
    }
}

// MARK: - AlbumCellDelegate
extension BookMarkVC: AlbumCellDelegate{
    func onTapBookmark(rowOfAlbum: Int) {
        albumModels[rowOfAlbum].isBookmarked = !albumModels[rowOfAlbum].isBookmarked
        NotificationCenter.post(customeNotification: TSNotification.removeBookmark, object: albumModels[rowOfAlbum])
        albumModels.remove(at: rowOfAlbum)
        albumTableView.reloadData()
        
    }
}
