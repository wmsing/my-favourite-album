//
//  TSErrorHandling.swift
//  favoritealbum
//
//  Created by tonysing on 29/10/2021.
//

import Foundation
import UIKit
import HandyJSON

struct TSErrorHandling: HandyJSON {
    var code: String?
    var message: String?
}

class TSError: NSObject {
    
    class func commonDisplay(_ error: (AnyObject), isShowDebugCode: Bool = false) {
        let errorObj = error as? TSErrorHandling
        let errorMsg = isShowDebugCode ? "Code = \(errorObj?.code ?? "00000" ) Message = \(errorObj?.message ?? "")" : ( errorObj?.message ?? "" )
        TSHud.showError(message: errorMsg)
        print("error: \(error)")
    }
}
