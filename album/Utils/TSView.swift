//
//  TSView.swift
//  favoritealbum
//
//  Created by tonysing on 29/10/2021.
//

import Foundation
import UIKit

let K_SCREEN_WIDTH = UIScreen.main.bounds.size.width
let K_SCREEN_HEIGHT = UIScreen.main.bounds.size.height
let k_SCREEN_WIDTH_COEFFICIENT_6 = K_SCREEN_WIDTH/375.0
let K_SCREEN_HEIGHT_COEFFICIENT_6 = K_SCREEN_HEIGHT/667.0
func parsePx(_ px: CGFloat) -> CGFloat { return px * (k_SCREEN_WIDTH_COEFFICIENT_6) / 2 }
func parsePt(_ pt: CGFloat) -> CGFloat { return pt * (k_SCREEN_WIDTH_COEFFICIENT_6) }

class TSView: NSObject {
    // MARK: - UIView
    class func view(frame: CGRect, bgColor: UIColor) -> UIView {
        let view = UIView(frame: frame)
        view.backgroundColor = bgColor
        return view
    }
    
    // MARK: - UILabel
    class func label(text: String, frame:CGRect, textColor: UIColor?, fontSize: CGFloat, align: NSTextAlignment?) -> UILabel {
        let label = UILabel(frame: frame)
        label.text = text
        if let _textColor = textColor {
            label.textColor = _textColor
        }
        if fontSize > 0 {
            label.font = UIFont.systemFont(ofSize: fontSize)
        }
        if let _textAlighment = align {
            label.textAlignment = _textAlighment
        }
        return label
    }
    
    // MARK: - UIImageView
    class func imageView(frame: CGRect, named: String) -> UIImageView {
        let imageView = UIImageView(frame:frame)
        imageView.image = UIImage(named:named)
        return imageView
    }
    
    class func imageView(frame: CGRect, image: UIImage) -> UIImageView {
        let imageView = UIImageView(frame: frame)
        imageView.image = image
        return imageView
    }
    
    
    // MARK: - Button
    class func button(type btnType: UIButton.ButtonType = .custom,
                      title: String?,
                      frame: CGRect,
                      titleColor: UIColor?,
                      font: CGFloat? = 14*k_SCREEN_WIDTH_COEFFICIENT_6,
                      backgroundColor: UIColor? = nil,
                      target: Any?,
                      action: Selector?,
                      textAlightment: NSTextAlignment?) -> UIButton{
        let button = UIButton(type: btnType)
        button.frame  = frame
        if title != nil {
            button.setTitle(title, for: .normal)
        }
        if backgroundColor != nil {
            button.backgroundColor = backgroundColor
        }
        if titleColor != nil {
            button.setTitleColor(titleColor, for: .normal)
        }
        if font != nil {
            button.titleLabel?.font = UIFont.systemFont(ofSize: font!)
        }
        if target != nil && action != nil {
            button.addTarget(target, action: action!, for: .touchUpInside)
        }
        if textAlightment != nil {
            button.titleLabel?.textAlignment = textAlightment!
        }
        return button
    }
}
