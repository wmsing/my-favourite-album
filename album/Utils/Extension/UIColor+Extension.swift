//
//  UIColor+Extension.swift
//  favoritealbum
//
//  Created by tonysing on 29/10/2021.
//

import Foundation
import UIKit

extension UIColor{
    //  MARK: - Theme Color
    static let themeFontBlack = UIColor.TSColorWithRGB(r: 34, g: 34, b: 34)
    static let themeFontGray = UIColor.TSColorWithRGB(r: 34, g: 34, b: 34)
    static let themeLightGray = UIColor.TSColorWithRGB(r: 160, g: 160, b: 160)
    static let themeBackgroundLightGray = UIColor(red: 142/255, green: 142/255, blue: 142/255, alpha: 0.12)
    static let themeRed = UIColor.TSColorWithRGB(r: 233, g: 55, b: 54)
    static let themeWhite = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1.0)
    static let themeLoadingBackground = UIColor.TSColorWithRGB(r: 44.0, g: 44.0, b: 49.0)

    class func TSColorWithRGB(r:CGFloat,g:CGFloat,b:CGFloat) -> UIColor {
        return UIColor(red: CGFloat(r)/255.0, green: CGFloat(g)/255.0, blue: CGFloat(b)/255.0, alpha: 1.0)
    }
    
}

