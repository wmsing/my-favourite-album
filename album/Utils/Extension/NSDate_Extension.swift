//
//  NSDate_Extension.swift
//  favoritealbum
//
//  Created by tonysing on 31/10/2021.
//

import Foundation
extension Date {
    func toString(date:Date, dateFormat:String, localeId: String) -> String {
        let formatter = DateFormatter()
        formatter.locale = Locale.init(identifier: localeId)
        formatter.dateFormat = dateFormat
        return formatter.string(from: date)
    }
    
    func toString(date:Date, dateFormat:String="yyyy-MM-dd") -> String {
        return toString(date: date, dateFormat: dateFormat, localeId: "Asia/Hong_Kong")
    }
    
}
