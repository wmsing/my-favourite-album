//
//  NotificationCenter+Extension.swift
//  favoritealbum
//
//  Created by tonysing on 30/10/2021.
//

import Foundation

extension NotificationCenter {
    static func post(customeNotification name: TSNotification, object: Any? = nil){
        NotificationCenter.default.post(name: name.notificationName, object: object)
    }
    
    static func addObserver(observer: Any, selector: Selector, name: TSNotification, object: Any? = nil) {
        NotificationCenter.default.addObserver(observer, selector: selector, name: name.notificationName, object: object)
    }
    
    static func removeObserver(observer: Any, name: TSNotification, object: Any? = nil) {
        NotificationCenter.default.removeObserver(observer, name: name.notificationName, object: object)
    }
}
