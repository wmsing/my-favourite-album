//
//  UIViewController+Extension.swift
//  favoritealbum
//
//  Created by tonysing on 29/10/2021.
//
import UIKit

extension UIViewController {
    
    // MARK: - Navigate
    func popToRootViewController() {
        weak var weakSelf = self
        weakSelf!.navigationController?.popToRootViewController(animated: true)
    }

    func setNavigationBarBackgroundColor(uiColor: UIColor)  {
        navigationController?.navigationBar.setBackgroundImage(creatImageWithColor(color: uiColor), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    func creatImageWithColor(color:UIColor)->UIImage{
        let rect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor)
        context!.fill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    typealias Task = (_ cancel : Bool) -> Void
    
    func delay(_ time: TimeInterval, task: @escaping ()->()) ->  Task? {
        
        func dispatch_later(block: @escaping ()->()) {
            let t = DispatchTime.now() + time
            DispatchQueue.main.asyncAfter(deadline: t, execute: block)
        }
        var closure: (()->Void)? = task
        var result: Task?
        
        let delayedClosure: Task = {
            cancel in
            if let internalClosure = closure {
                if (cancel == false) {
                    DispatchQueue.main.async(execute: internalClosure)
                }
            }
            closure = nil
            result = nil
        }
        
        result = delayedClosure
        
        dispatch_later {
            if let delayedClosure = result {
                delayedClosure(false)
            }
        }
        return result
    }
    
    func cancel(_ task: Task?) {
        task?(true)
    }
    
    
    @discardableResult
    func createNavigationItemWithTitle(str: String,
                                       imageName: String,
                                       selector: Selector? ,
                                       isLeft: Bool ) -> UIButton {
        let btn = TSView.button(title: str,
                                           frame: CGRect(x: 0, y: 20, width: 34, height: 44),
                                           titleColor: .black,
                                           target: self,
                                           action: selector,
                                textAlightment: .center)
        
        if "" != imageName {
            btn.setImage(UIImage.init(named: imageName), for: .normal)
        }
        if isLeft{
            self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(customView: btn)
        } else {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(customView: btn)
        }
        
        return btn
    }
    
    func navBarHeight()->CGFloat{
        return navigationController?.navigationBar.frame.size.height ?? 44;
    }
}
 

