//
//  TSDateTime.swift
//  favoritealbum
//
//  Created by tonysing on 31/10/2021.
//

import Foundation
class TSDateTime{
    static func isoDateToYyMmDd(from isoDate: String) -> String{
        let dateFormatter = ISO8601DateFormatter()
        guard let date = dateFormatter.date(from:isoDate) else { return "-" }
        return date.toString(date: date)
    }
}
