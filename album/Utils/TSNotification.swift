//
//  TSNotification.swift
//  favoritealbum
//
//  Created by tonysing on 30/10/2021.
//

import Foundation

enum TSNotification: String {
    case removeBookmark

    var stringValue: String {
        return "TS" + rawValue
    }
    
    var notificationName: NSNotification.Name {
        return NSNotification.Name(stringValue)
    }
}

