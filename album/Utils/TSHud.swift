//
//  TSHud.swift
//  favoritealbum
//
//  Created by tonysing on 29/10/2021.
//

import UIKit
import SVProgressHUD

class TSHud: NSObject {
    static let backgroundColor = UIColor.themeLoadingBackground
    
    static func loading(message: String = "Loading...") {
        SVProgressHUD.setBackgroundColor(backgroundColor)
        var mutArr = [UIImage]()
        for index in 1...8 {
            guard let image = UIImage(named: "loading0\(index)") else { continue }
            mutArr.append(image)
        }
        if mutArr.count == 0 {
            SVProgressHUD.setForegroundColor(.white)
            SVProgressHUD.show()
        } else {
            SVProgressHUD.show(UIImage.animatedImage(with: mutArr, duration: 1.6)!, status: nil)
            SVProgressHUD.setImageViewSize(CGSize(width: parsePx(80), height: parsePx(80)))
        }
        SVProgressHUD.setMaximumDismissTimeInterval(60)
    }
    
    static func disMiss() {
        SVProgressHUD.dismiss()
    }
    
    static func showError (message:String) {
        loading(message: message);
    }
    
}

