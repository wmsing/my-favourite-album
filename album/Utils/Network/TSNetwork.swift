//
//  TSNetwork.swift
//  favoritealbum
//
//  Created by tonysing on 29/10/2021.
//

import UIKit
import Alamofire

class TSNetwork: NSObject {
    
    struct responseData {
        var request:URLRequest?
        var response:HTTPURLResponse?
        var json:AnyObject?
        var error:NSError?
        var data:Data?
    }
    
    static let baseUrl = "https://itunes.apple.com/"
    
    //    HTTP GET
    class func get(_ url:String,
                   para:[String:Any]?,
                   showHud: Bool = false,
                   success: @escaping (AnyObject)->Void,
                   fail: @escaping(_ error :AnyObject)->Void,
                   complete: @escaping()->Void) {
        return requestWith(.get, url, Parameter: para, bodyISJson: false, showHud: showHud, success: success, fail: fail, complete: complete)
    }
    
    //    HTTP GET
    class func get(_ url:String,
                   showHud: Bool = true,
                   success: @escaping (AnyObject)->Void,
                   fail: @escaping(_ error :AnyObject)->Void,
                   complete: @escaping()->Void) {
        return requestWith(.get, url, Parameter: nil, bodyISJson: false, showHud: showHud,  success: success, fail: fail, complete: complete)
    }
    
    //    HTTP POST
    class func post(_ url:String,
                    parameter: [String:Any]?,
                    showHud: Bool = true,
                    success: @escaping (AnyObject)->Void,
                    fail :@escaping(_ error :AnyObject)->Void,
                    complete: @escaping()->Void) {
        
        return requestWith(.post, url, Parameter: parameter, bodyISJson: true, showHud: showHud, success: success, fail: fail, complete: complete)
    }
    
    //    HTTP PUT
    class func put(_ url:String,
                   parameter: [String:Any]?,
                   showHud: Bool = true,
                   success: @escaping (AnyObject)->Void,
                   fail :@escaping(_ error :AnyObject)->Void,
                   complete: @escaping()->Void) {
        return requestWith(.put, url, Parameter: parameter, bodyISJson: true, showHud: showHud, success: success, fail: fail, complete: complete)
    }
    
    //    HTTP DELETE
    class func delete(_ url:String,
                      parameter: [String:Any]?,
                      showHud: Bool = true,
                      success: @escaping (AnyObject)->Void,
                      fail :@escaping(_ error :AnyObject)->Void,
                      complete: @escaping()->Void) {
        return requestWith(.delete, url, Parameter: parameter, bodyISJson: true,
                           showHud: showHud, success: success, fail: fail, complete: complete)
    }
    
    class func requestWith(_  method:Alamofire.HTTPMethod,
                           _  url:String,
                           Parameter para:[String:Any]?,
                           bodyISJson:Bool,
                           showHud: Bool = false,
                           success: @escaping (AnyObject) ->Void,
                           fail:@escaping ( _ error :AnyObject) ->Void,
                           complete: @escaping()->Void) {
        if !TSNetworkReachability.shared.reachable {
            return
        }
        //        let secret = "xx"
        let headers: HTTPHeaders = [
            "Accept": "application/json",
            "request-time": TSNetwork.getRequestTime(),
            "Content-Type":"application/json"
            //            "Authorization": "Bearer xxx"
        ]
        let manager = Alamofire.Session.default
        manager.session.configuration.timeoutIntervalForRequest = 10
        manager.session.configuration.timeoutIntervalForResource = 10
        
        print("__Request__ >>>>>>>>>>>>>>>>>>>>>>>>>>>>")
        var requestUrl = url
        if !url.contains("http") {
            requestUrl = baseUrl + url
        }
        manager.request(requestUrl, method: method, parameters:para , encoding: bodyISJson ? JSONEncoding.default : URLEncoding.default, headers:headers).response(completionHandler: { (response) in
            complete()
            var errorObj = TSErrorHandling()
            //            errorObj.code = "99999"
            guard let _ = response.response?.statusCode else {
                errorObj.message = "Connection Error"
                fail(errorObj as AnyObject)
                return
            }
            guard let data = response.data else {
                errorObj.message = "invalid response data -- 1"
                fail(errorObj as AnyObject)
                return     //
            }
            guard let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as AnyObject else {
                errorObj.message = "Serialization Error -- 1"
                fail(errorObj as AnyObject)
                return    // parse json error
            }
            
            if response.error != nil {
                //json = nil throw error
                guard let _ = response.response else{
                    fail("502" as AnyObject)
                    return
                }
                fail("503" as AnyObject)
            }
            
            //   Result ok
            let res = responseData(request:response.request, response:response.response, json:json, error:response.error as NSError?, data: data)
            guard let dic = res.json as? NSDictionary else {
                fail("501" as AnyObject)
                return
            }
            
            print("__Result__ url: \(requestUrl) <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
            
            //    Success result
            success(dic as AnyObject)
        })
    }
    
    /// Set Request TimeZone to Hong_Kong
    static func getRequestTime() -> String{
        let date = Date.init()
        let timeFormatter = DateFormatter.init()
        timeFormatter.timeZone = TimeZone(identifier: "Asia/Hong_Kong")
        timeFormatter.dateFormat="yyyy-MM-dd HH:mm:ss"
        return timeFormatter.string(from: date)
    }
}
