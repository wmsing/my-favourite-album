//
//  TSNetworkReachability.swift
//  favoritealbum
//
//  Created by tonysing on 29/10/2021.
//

import Foundation
import Alamofire

class TSNetworkReachability: NSObject {
    
    static let shared = TSNetworkReachability()
    
    var reachable:Bool = {
        guard let manager = Alamofire.NetworkReachabilityManager(host: "www.google.com") else {
            return false
        }
        if(manager.isReachable||manager.isReachableOnEthernetOrWiFi){
            return true
        }
        return false
    }()
}
