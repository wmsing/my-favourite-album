# My Favourite Album
To demostrate my iOS knowledge.

## Features
1. Show a list of album fetched from (https://itunes.apple.com/search?term=jack+johnson&entity=album)
1. Let user bookmark albums
1. Show a list of bookmarked albums

## Installation
Use the CocoaPods(https://cocoapods.org/) dependency manager to install third-party library. 
```bash
pod install 
```

## Usage
open album.xcworkspace

## List of third-party library
1. SnapKit - Add constrain to UIView
1. Alamofire - Fetch api
1. SVProgressHUD - Loading Widget
1. Kingfisher - Network Image
1. HandyJSON - Convert Json to model object
1. DZNEmptyDataSet - Empty image for UITableView or UICollectionView


## License
[MIT](https://choosealicense.com/licenses/mit/)


